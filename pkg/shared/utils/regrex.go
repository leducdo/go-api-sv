package utils

import "regexp"

// regular expressions for validating string
const (
	alphaNumericRegexString string = `^[a-zA-Z0-9]+$`
)

var (
	AlphaNumericRegex = regexp.MustCompile(alphaNumericRegexString)
)
