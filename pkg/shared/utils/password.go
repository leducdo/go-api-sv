package utils

import "golang.org/x/crypto/bcrypt"

/**
 * HashPassword hash password
 * @param password Password need hash
 * @return hashed password, error
 */
func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return string(bytes), err
}

/**
 * CheckPasswordHash check password
 * @param password Password need check
 * @param hash Hashed Password
 * @return bool true is equal and false is not eaqual
 */
func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
