package validator

import (
	"go-api-sv/pkg/shared/utils"

	v "github.com/go-playground/validator/v10"
)

func IsAlphaNumericType(fl v.FieldLevel) bool {
	value := fl.Field().String()

	return utils.AlphaNumericRegex.MatchString(value)
}
